type InventoryItem = {
  id: string;
  sku: string;
  description: string;
  price1: number;
  price2: number;
  price3: number;
  price4: number;
  price5: number;
  price6: number;
  taxType: TaxType;
  binLocation?: string;
  stockAvailability: StockAvailability;
};

type StockAvailability = {
  onSalesOrder: number;
  onHand: number;
  onPurchaseOrder: number;
};

type TaxType = {
  id: string;
  name: string;
  rate: number;
};

type Customer = {
  id: string;
  name: string;
  addressLine1?: string;
  addressLine2?: string;
  addressLine3?: string;
  postalCode?: string;
  priceType: number;
  taxNumber?: string;
  paymentTerms: PaymentTerms;
  active: boolean;
  onHold: boolean;
};

type PaymentTerms = {
  id: string;
  name: string;
};

type StockLocation = {
  id: string;
  name: string;
};

type SalesRep = {
  id: string;
  name: string;
};

type Quotation = {
  id: string;
  date: Date;
  customer: Customer;
  salesRep: SalesRep;
  reference: string;
  lineItems: LineItem[];
  totalExcludingTax: number;
  totalTax: number;
  totalIncludingTax: number;
};

type LineItem = {
  inventoryItem: InventoryItem;
  quantity: number;
  unitPrice: number;
  taxType: TaxType;
  totalExcludingTax: number;
  totalTax: number;
  totalIncludingTax: number;
};
